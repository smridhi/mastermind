import requests as rq
import json
def get_url(word_1):
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"

    register_dict = {"mode" : "Mastermind" , "name" : "WEBOT"}
    register_with = json.dumps(register_dict)

    register_url = mm_url +"register"
    r = rq.post(register_url, json=register_dict)
    r.json()['id']
    me = r.json()['id']
    creat_url = mm_url + "create"
    creat_dict = {"id":me, "overwrite":True}
    rc = rq.post(creat_url, json= creat_dict)
    session = rq.Session()
    resp = session.post(register_url, json=register_dict)
    me = resp.json()['id']
    creat_dict = {'id': me, "overwrite": True}
    rc = session.post(creat_url, json=creat_dict)
    guess = {"id": me, "guess": word_1}
    guess_url = mm_url + "guess"
    correct = session.post(guess_url, json=guess)
    return (correct.json())

print(get_url())
