import sys
from itertools import permutations as nPr
from api import get_url

def load_word_list(file_path: str) -> list[str]:
	with open(file_path, 'r') as file:
		words = file.read().splitlines()
	return words

all_words = load_word_list("5letters.txt")

def feedback(word: str) -> int:
	return 0

def generate_guess(word: str) -> str:
	n = feedback(word)
	if n == 0:
		delete_no_occurances(word)
	elif n == 5:
		for op in (feedback([word for word in generate_anagrams(word)])):
			if "win" in op:
				exit
	else:
		return generate_guess(op for op in (feedback([word for word in feedback_review(word)])))

def delete_no_occurances(word: str) -> list[str]:
	for s in word:
		if s in all_words:
			all_words.remove(s)
	return all_words

def feedback_review(word: str) -> list[str]:
        num = feedback(word)
	permutations = list(set(nPr(word, num)))
	possibles = [''.join(p) for p in permutations]
	return possibles

def generate_anagrams(word: str, alpha: list[str]) -> list[str]:
	permutations = set(nPr(word, 5))
	anagrams = [''.join(p) for p in permutations]
	return anagrams
