import requests as rq
import json

mm_url = "https://we6.talentsprint.com/wordle/apidocs/#/"

# Register endpoint URL
register_url = mm_url + "register/"
register_dict = {"mode": "Mastermind", "name": "Khushi"}


register_with = json.dumps(register_dict)
print("Register payload (JSON):", register_with)


r = rq.post(register_url, json=register_dict)


print("Response Text:")
print(r.text)

if r.status_code == 201:  # Assuming 201 Created is the success status code
    # Parse the JSON response
    response_json = r.json()
    print("Parsed JSON Response:", response_json)

    # Extract the ID from the JSON response
    id_value = response_json.get('id')
    print("Extracted ID:", id_value)

    # Store the ID in a variable
    me = id_value
else:
    print(f"Request failed with status code: {r.status_code}")

# Create endpoint URL
create_url = mm_url + "create"
create_dict = {
    
    "id": me,
    "some_other_key": "some_value"
}

# Send POST request to create endpoint
create_response = rq.post(create_url, json=create_dict)

# Print the raw response text
print("Create Response Text:")
print(create_response.text)

# Check if the create request was successful
if create_response.status_code == 201:  # Assuming 201 Created is the success status code
    # Parse the JSON response
    create_response_json = create_response.json()
    print("Parsed Create JSON Response:", create_response_json)
else:
    print(f"Create request failed with status code: {create_response.status_code}")
